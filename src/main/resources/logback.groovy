/*
 * logback.groovy
 *
 * Configure our script to log to the console.
 */

appender("CONSOLE", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%date{ISO8601} %-5level : %msg%n"
    }
}

root(DEBUG, ["CONSOLE"])

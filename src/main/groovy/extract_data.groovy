import gate.*
import gate.corpora.RepositioningInfo
import gate.util.Out
import groovy.json.JsonBuilder
import textgate.*

import java.nio.charset.Charset

def cli = new CliBuilder(usage: 'textgate -i [input] -o [outputdir]',
        header: 'Options:')
cli._(longOpt: 'inputFile', args: 1, argName: 'inputFile', 'Use given input file')
cli._(longOpt: 'outputDir', args: 1, argName: 'outputDir', 'Use given output directory')
def options = cli.parse(args)

assert options.inputFile
File inputFile = new File(options.inputFile)
assert inputFile.exists()

assert options.outputDir
File outputDir = new File(options.outputDir)
assert outputDir.exists() && outputDir.canWrite()

// Load up gate.
GateSingleton.getInstance()

Extractor annie = new Extractor()
annie.initAnnie()

String contents = Files.readFile(inputFile, Charset.forName('UTF-8'))

Corpus corpus = Factory.newCorpus("StandAloneAnnie corpus")

// We could add more documents to the corpus if we wanted here.
FeatureMap params = Factory.newFeatureMap()
params.put(Document.DOCUMENT_PRESERVE_CONTENT_PARAMETER_NAME, true)
params.put(Document.DOCUMENT_REPOSITIONING_PARAMETER_NAME, true)
params.put(Document.DOCUMENT_STRING_CONTENT_PARAMETER_NAME, contents)
params.put(Document.DOCUMENT_MIME_TYPE_PARAMETER_NAME, 'text/html')
params.put(Document.DOCUMENT_MARKUP_AWARE_PARAMETER_NAME, true)
Document corpusDoc = (Document) Factory.createResource("gate.corpora.DocumentImpl", params)
corpus.add(corpusDoc)

// tell the pipeline about the corpus and run it
annie.setCorpus(corpus)
annie.execute()

// for each document, get an XML document with the
// person and location names added
Iterator iter = corpus.iterator()
int count = 0
String startTagPart_1 = "<span GateID=\""
String startTagPart_2 = "\" title=\""
String startTagPart_3 = "\" style=\"background:Red;\">"
String endTag = "</span>"

while (iter.hasNext()) {
    Document doc = (Document) iter.next()
    AnnotationSet defaultAnnotSet = doc.getAnnotations()
    Set annotTypesRequired = ['Person', 'Date', 'Title', 'Organization'] as Set
    Set<Annotation> peopleAndPlaces = new HashSet<Annotation>(defaultAnnotSet.get(annotTypesRequired))

    FeatureMap features = doc.getFeatures()
    String originalContent = (String) features.get(GateConstants.ORIGINAL_DOCUMENT_CONTENT_FEATURE_NAME)
    RepositioningInfo info = (RepositioningInfo) features.get(GateConstants.DOCUMENT_REPOSITIONING_INFO_FEATURE_NAME)

    count += 1

    List<Person> personList = []
    List<Title> titleList = []
    List<Organization> organizationList = []
    List<Date> dateList = []

    if (originalContent != null && info != null) {
        Iterator it = peopleAndPlaces.iterator()
        Annotation currAnnot
        SortedAnnotationList sortedAnnotations = new SortedAnnotationList()

        while (it.hasNext()) {
            currAnnot = (Annotation) it.next()
            sortedAnnotations.addSortedExclusive(currAnnot)
        }

        StringBuffer editableContent = new StringBuffer(originalContent)
        long insertPositionEnd
        long insertPositionStart

        // insert annotation tags backward (otherwise our ordering will be jacked up)
        for (int i = sortedAnnotations.size() - 1 ; i >= 0 ; --i) {
            currAnnot = (Annotation) sortedAnnotations.get(i)
            insertPositionStart = currAnnot.getStartNode().getOffset().longValue()
            insertPositionStart = info.getOriginalPos(insertPositionStart)
            insertPositionEnd = currAnnot.getEndNode().getOffset().longValue()
            insertPositionEnd = info.getOriginalPos(insertPositionEnd, true)


            if (insertPositionEnd != -1 && insertPositionStart != -1) {
                String chunk = editableContent.substring((int) insertPositionStart, (int) insertPositionEnd)
                chunk = org.apache.commons.lang.StringEscapeUtils.unescapeHtml(chunk)
                chunk = Markups.html2text(chunk).trim()
                Out.prln("CHUNK=" + chunk)
                Out.prln("TYPE=" + currAnnot.getType())
                Out.prln("ANNOTATION=" + currAnnot)

                if (currAnnot.getType() == 'Title') {
                    titleList += new Title(title: chunk)
                } else if (currAnnot.getType() == 'Person') {
                    Gender gender = null
                    if ('female' == currAnnot.getFeatures().get('gender')) {
                        gender = Gender.FEMALE
                    } else if ('male' == currAnnot.getFeatures().get('gender')) {
                        gender = Gender.MALE
                    }
                    personList += new Person(name: chunk, gender: gender)
                } else if (currAnnot.getType() == 'Organization') {
                    organizationList += new Organization(name: chunk)
                } else if (currAnnot.getType() == 'Date') {
                    dateList += new Date(date: chunk)
                }

                editableContent.insert((int) insertPositionEnd, endTag)
                editableContent.insert((int) insertPositionStart, startTagPart_3)
                editableContent.insert((int) insertPositionStart, currAnnot.getType())
                editableContent.insert((int) insertPositionStart, startTagPart_2)
                editableContent.insert((int) insertPositionStart, currAnnot.getId().toString())
                editableContent.insert((int) insertPositionStart, startTagPart_1)
            }
        }

        new File(outputDir, 'markup.html').write(editableContent.toString())
    }

    new File(outputDir, 'annotations.xml').write(doc.toXml(peopleAndPlaces, false))

    personList.unique { a, b -> a.name <=> b.name }
    titleList.unique { a, b -> a.title <=> b.title }
    organizationList.unique { a, b -> a.name <=> b.name }
    dateList.unique { a, b -> a.date <=> b.date }

    Out.prln("PERSONS=" + personList)
    Out.prln("TITLES=" + titleList)
    Out.prln("Organizations=" + organizationList)
    Out.prln("Dates=" + dateList)

    new File(outputDir, 'person.json').write(new JsonBuilder(personList).toString())
    new File(outputDir, 'title.json').write(new JsonBuilder(titleList).toString())
    new File(outputDir, 'organization.json').write(new JsonBuilder(organizationList).toString())
    new File(outputDir, 'dates.json').write(new JsonBuilder(dateList).toString())

}


package textgate

import org.jsoup.Jsoup

/**
 * Markup utilities.
 */
class Markups {

    static String html2text(String html) {
        Jsoup.parse(html).text()
    }
}

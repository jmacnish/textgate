package textgate

import groovy.transform.Canonical

/**
 * Simple person.
 */
@Canonical
class Person {
    String name
    Gender gender

}

package textgate

import groovy.transform.Canonical

/**
 * Simple job title
 */
@Canonical
class Title {
    String title
}

package textgate

import groovy.transform.Canonical

/**
 * Created by jmacnish on 4/5/14.
 */
@Canonical
class Date {
    String date
}

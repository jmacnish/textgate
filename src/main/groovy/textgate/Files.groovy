package textgate

import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.nio.file.Paths

/**
 * File utilities.
 */
class Files {

    static String readFile(File file, Charset encoding) throws IOException {
        readFile(file.getAbsolutePath(), encoding)
    }

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = java.nio.file.Files.readAllBytes(Paths.get(path))
        encoding.decode(ByteBuffer.wrap(encoded)).toString()
    }
}

package textgate

import groovy.transform.Canonical

/**
 * Simple company.
 */
@Canonical
class Organization {
    String name

}

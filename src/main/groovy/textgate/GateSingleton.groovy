package textgate

import gate.Gate
import gate.util.GateException
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Initializes gate.
 */
class GateSingleton {
    private static GateSingleton instance = null;
    private static Gate gate = null;
    private static Logger logger = LoggerFactory.getLogger(this)
    // Fixed location for Gate -- improve.
    private static String GateHome = '/usr/local/gate/gate-7.1-build4485-BIN/'

    protected GateSingleton() throws GateException, IOException {
        logger.info("Initializing GATE");
        gate = new Gate()
        gate.init()
        logger.info("Loading ANNIE Plugin")
        File gateHome = new File(GateHome)
        File pluginsHome = new File(gateHome, "plugins")
        gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "ANNIE").toURL())
        gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "Tools").toURL())
        gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "Annotation_Merging").toURL())
        logger.info("Done Initializing GATE");
    }

    public static GateSingleton getInstance() throws GateException, IOException {
        if (instance == null) {
            logger.info("Creating Gate Loader");
            instance = new GateSingleton();
        }
        logger.info("Returning Gate Loader Instance");
        return instance;
    }
}

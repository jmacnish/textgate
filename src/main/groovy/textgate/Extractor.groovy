package textgate

import gate.*
import gate.creole.RealtimeCorpusController
import gate.util.GateException
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by jmacnish on 4/5/14.
 */
class Extractor {
    RealtimeCorpusController annieController;
    private static Logger logger = LoggerFactory.getLogger(this)
    ProcessingResource annotpr
    ProcessingResource tokeniser
    ProcessingResource bgazetteer
    ProcessingResource split
    ProcessingResource postagger
    ProcessingResource transducer
    ProcessingResource orthoMatcher

    public void initAnnie() throws GateException {
        logger.info("Initialising ANNIE...")
        annieController = (RealtimeCorpusController) Factory.createResource(
                "gate.creole.RealtimeCorpusController",
                Factory.newFeatureMap(),
                Factory.newFeatureMap(),
                "ANNIE_" + Gate.genSym()
        );
        annotpr = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR", Factory.newFeatureMap())
        annieController.add(annotpr)

        tokeniser = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser", Factory.newFeatureMap())
        annieController.add(tokeniser)

        FeatureMap gazFeatures = Factory.newFeatureMap()
        gazFeatures.put("wholeWordsOnly", true)
        gazFeatures.put("longestMatchOnly", true)
        bgazetteer = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer", gazFeatures)
        annieController.add(bgazetteer)

        split = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter", Factory.newFeatureMap())
        annieController.add(split)

        postagger = (ProcessingResource) Factory.createResource("gate.creole.POSTagger", Factory.newFeatureMap())
        annieController.add(postagger)

        ProcessingResource morpho = (ProcessingResource) Factory.createResource("gate.creole.morph.Morph", Factory.newFeatureMap())
        annieController.add(morpho)

        transducer = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", Factory.newFeatureMap())
        annieController.add(transducer)

        orthoMatcher = (ProcessingResource) Factory.createResource("gate.creole.orthomatcher.OrthoMatcher", Factory.newFeatureMap())
        annieController.add(orthoMatcher)

        logger.info("...ANNIE loaded")
    }

    public void setCorpus(Corpus corpus) {
        annieController.setCorpus(corpus)
    }

    public void execute() throws GateException {
        annieController.execute()
    }

    public void cleanUp() {
        Corpus corp = annieController.getCorpus()
        if (!corp.isEmpty()) {
            for (int i = 0; i < corp.size(); i++) {
                Document doc1 = (Document) corp.remove(i)
                corp.unloadDocument(doc1)
                Factory.deleteResource(corp)
                Factory.deleteResource(doc1)
            }
        }
    }
}

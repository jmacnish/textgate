package textgate

/**
 * Male/female gender.
 */
enum Gender {
    MALE, FEMALE
}

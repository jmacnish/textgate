textgate
========

Requirements
------------

1. Get the binary for GATE with the current version "gate-7.1-build4485-BIN" from https://gate.ac.uk/

2. Install gate-7.1-build4485-BIN into /usr/local/gate

3. Gate should exist as a directory: "/usr/local/gate/gate-7.1-build4485-BIN/"

4. Set some defaults for Gate:


    export JAVA_OPTS="-Dgate.site.config=/usr/local/gate/gate-7.1-build4485-BIN/gate.xml -Dgate.plugins.home=/usr/local/gate/gate-7.1-build4485-BIN/plugins"

Usage
-----

Basic usage:

    /usr/local/textgate/bin/textgate --inputFile=[input HTML file] --outputDir=[directory to create output in]

The results of the extraction show up as split JSON/XML files:

    -rw-r--r-- 1 tomcat tomcat 41195 Apr  8 01:22 annotations.xml
    -rw-r--r-- 1 tomcat tomcat    33 Apr  8 01:22 dates.json
    -rw-r--r-- 1 tomcat tomcat 37162 Apr  8 01:21 input.html
    -rw-r--r-- 1 tomcat tomcat 48756 Apr  8 01:22 markup.html
    -rw-r--r-- 1 tomcat tomcat   927 Apr  8 01:22 organization.json
    -rw-r--r-- 1 tomcat tomcat  1865 Apr  8 01:22 person.json
    -rw-r--r-- 1 tomcat tomcat   229 Apr  8 01:22 title.json


